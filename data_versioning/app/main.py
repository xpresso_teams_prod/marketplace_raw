"""
    Fetches Data either using Data Connector or Request Manager
"""

import logging
import os
import shutil
import time

import click
import xpresso.ai.core.data.versioning.controller_factory as \
    version_controller_factory
from xpresso.ai.client.controller_client import ControllerClient
from xpresso.ai.client.data_client import config
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    CLICommandFailedException
from xpresso.ai.core.commons.utils.constants import KEY_RUN_NAME, COMPONENT_NAME_KEY
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ["DataVersioning"]
__author__ = "Ashritha Goramane"

logger = XprLogger("data_versioning", level=logging.INFO)


def move_directory(source, destination):
    """Move directory from source to destination. Overwrites
        if exists"""
    destination_folder = os.path.join(destination, os.path.basename(source))
    if os.path.exists(destination_folder):
        print(f"Overwriting {destination_folder}")
        os.system(f"rm -rf {destination_folder}")
    try:
        shutil.move(source, destination)
        return True
    except OSError:
        print("File exists in destination path")
        return False


class DataVersioning(AbstractPipelineComponent):
    """
        To push and pull data from pachyderm cluster
    """
    COMMAND_KEY_WORD = "command"
    DATASET_NAME = 'dataset_name'
    DATASET = "dataset"
    REPO_NAME = "repo_name"
    BRANCH_NAME = "branch_name"
    COMMIT_ID = "commit_id"
    DESCRIPTION = "description"
    IN_PATH = "in_path"
    OUT_PATH = "out_path"
    ENV = "environment"
    TYPE = "type"

    def __init__(self, **kwargs):
        super().__init__(name="DataVersioning")
        self.name = kwargs[COMPONENT_NAME_KEY]
        self.cli_args = {}
        self.arguments = kwargs
        self.fetch_arguments()
        self.supported_commands = {}
        self.initialize_commands()
        self.repomanager = None
        self.xpr_con = None
        self.config = config
        self.start_timestamp = None
        self.end_timestamp = None

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data
        preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the
          Controller that
              the component has started processing (details such as
              the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
                identify the current run. It must be passed. While
                running as
                pipeline,
               Xpresso automatically adds it.
        """
        super().start(xpresso_run_name=xpresso_run_name)
        print("Data versioning component starting", flush=True)
        self.execute()
        print("Data versioning component completed", flush=True)
        self.completed(push_exp=False)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the
        end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        super().completed(push_exp=push_exp)

    def send_metrics(self, status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": status},
            "metric": {
                "elapsed_time": self.end_timestamp - self.start_timestamp}
        }
        self.report_status(status=report_status)

    def execute(self):
        """
        Validates the command provided and calls the relevant function for
        execution
        """
        command = self.cli_args[self.COMMAND_KEY_WORD]
        if not command:
            raise CLICommandFailedException("No valid command provided")

        if command not in self.supported_commands:
            raise CLICommandFailedException("Invalid command found")
        try:
            self.fetch_arguments()
            result = self.supported_commands[command]()
            return result
        except TypeError as exception:
            raise CLICommandFailedException(
                "Command cannot be executed {}".format(exception))

    def extract_argument(self, argument):
        """
        Args:
            argument(str): Name of argument to extract
        Returns:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def initialize_commands(self):
        """
        Initialize the supported command variable
        """
        self.supported_commands = {
            "pull_dataset": self.pull_dataset,
            "push_dataset": self.push_dataset
        }

    def validate_push_dataset_input(self):
        """Validates push dataset input arguments """
        if not self.cli_args[self.IN_PATH]:
            self.cli_args[self.IN_PATH] = "/data"
        mandatory_fields = [self.REPO_NAME, self.BRANCH_NAME, self.TYPE,
                            self.DATASET_NAME, self.IN_PATH, self.DESCRIPTION,
                            self.ENV]
        for field in mandatory_fields:
            if not self.cli_args[field]:
                print(f"{field} not provided.")

    def push_dataset(self):
        """Push dataset onto pachyderm repo"""
        self.start_timestamp = time.time()
        password = self.config.PASSWORD
        uid = self.config.UID
        self.version_connect(uid, password, self.cli_args[self.ENV])

        if self.cli_args[self.TYPE] == 'dataset':
            print("Pushing dataset onto repo")
            new_commit_id, path_on_cluster = self.repomanager.push_dataset(
                repo_name=self.cli_args[self.REPO_NAME],
                branch_name=self.cli_args[self.BRANCH_NAME],
                dataset=self.cli_args[self.DATASET],
                description=self.cli_args[self.DESCRIPTION])

        else:
            self.validate_push_dataset_input()
            print("Pushing files onto repo")
            new_commit_id, path_on_cluster = self.repomanager.push_dataset(
                repo_name=self.cli_args[self.REPO_NAME],
                branch_name=self.cli_args[self.BRANCH_NAME],
                dataset_name=self.cli_args[self.DATASET_NAME],
                path=self.cli_args[self.IN_PATH],
                description=self.cli_args[self.DESCRIPTION],
                data_type=self.cli_args[self.TYPE])
        print(f"Push dataset details: {new_commit_id}, {path_on_cluster}")
        self.end_timestamp = time.time()
        self.send_metrics("push_dataset")
        return new_commit_id, path_on_cluster

    def pull_dataset(self):
        """Pulls the data from the pachyderm repo"""
        self.start_timestamp = time.time()
        password = self.config.PASSWORD
        uid = self.config.UID

        self.version_connect(uid, password, self.cli_args[self.ENV])
        new_dir_path = self.repomanager.pull_dataset(
            repo_name=self.cli_args[self.REPO_NAME],
            branch_name=self.cli_args[self.BRANCH_NAME],
            commit_id=self.cli_args[self.COMMIT_ID],
            output_type="files")
        if self.cli_args[self.IN_PATH]:
            new_dir_path = os.path.join(new_dir_path, self.cli_args[self.IN_PATH][1:])
        move_directory(new_dir_path, self.cli_args[self.OUT_PATH])
        os.system(f'rm -rf {self.cli_args[self.COMMIT_ID]}')
        self.end_timestamp = time.time()
        self.send_metrics("pull_dataset")

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        Returns:
            Returns arguments
        """
        arguments_key = [self.COMMAND_KEY_WORD, self.REPO_NAME,
                         self.BRANCH_NAME, self.ENV, self.COMMIT_ID,
                         self.OUT_PATH, self.IN_PATH, self.DESCRIPTION,
                         self.DATASET_NAME, self.DATASET, self.TYPE]
        for arg in arguments_key:
            self.cli_args[arg] = self.extract_argument(arg)

    def version_connect(self, uid, passw, env):
        """Login to xpresso"""
        if not env:
            print(f"Unable to login. Please specify env.")
        try:
            os.system("echo -n {} > ~/.xpr/.workspace".format("default"))
            client = ControllerClient()
            client.login(uid, passw)
            controller_factory = \
                version_controller_factory.VersionControllerFactory()
            self.repomanager = controller_factory.get_version_controller()
        except Exception as exp:
            raise exp


@click.command()
@click.argument(KEY_RUN_NAME)
@click.argument(COMPONENT_NAME_KEY)
@click.option('-command', type=str, help='Versioning command to execute')
@click.option('-repo-name', type=str, help='Name of the repo')
@click.option('-branch-name', type=str, help='Branch name')
@click.option('-commit-id', type=str, help='Commit Id')
@click.option('-dataset', help='Dataset object')
@click.option('-dataset-name', help="Name of the dataset that will be pushed")
@click.option('-out-path', type=str,
              help='Path where you want to pull')
@click.option('-in-path', type=str,
              help='Path of the file you want to fetch')
@click.option('-description', type=str, help='Description regarding the '
                                             'function to perform')
@click.option('-type', type=str, help='Source where to pick data from',
              default="files")
@click.option('-env', '--environment', type=str, help="Workspace on xpresso")
def cli_options(**kwargs):
    result = DataVersioning(**kwargs)
    try:
        if KEY_RUN_NAME in kwargs:
            result.start(xpresso_run_name=kwargs[KEY_RUN_NAME])
        else:
            result.start(xpresso_run_name="")
    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    cli_options()
